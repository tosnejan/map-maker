import ctypes
import os
import platform
from tkinter import Tk
from tkinter.filedialog import askopenfilename, asksaveasfilename

import pygame

# Size of the screen
tk = Tk()
SCREEN_TITLE = "Map Maker"
SCREEN_WIDTH = tk.winfo_screenwidth()
SCREEN_HEIGHT = tk.winfo_screenheight()
RATIO = (1920 / SCREEN_WIDTH, 1080 / SCREEN_HEIGHT)
os.environ['SDL_VIDEO_WINDOW_POS'] = "0,0"
# Colors according to RGB codes
WHITE_COLOR = (255, 255, 255)
BLACK_COLOR = (0, 0, 0)
MAXIMAL_ZOOM = 4.5
# Clock used to update game events and frames
clock = pygame.time.Clock()
pygame.font.init()
font = pygame.font.SysFont("Sans", 75)


def scale(surf):
    return pygame.transform.scale(surf, (SCREEN_WIDTH, SCREEN_HEIGHT))


class Game:
    # Typical rate of 60, equivalent to FPS
    TICK_RATE = 60

    def __init__(self, image_path, title, width, height):
        self.title = title
        self.width = width
        self.height = height

        # Create the window of specified size in white to display the game
        self.screen = pygame.display.set_mode((width, height))
        # Set the game window color to white
        self.screen.fill(WHITE_COLOR)
        pygame.display.set_caption(title)

        background_image = pygame.image.load(image_path)
        self.image = pygame.transform.scale(background_image, (width, height))

    def run_game_loop(self):
        running = True
        in_menu = False
        in_book = False
        menu = Menu()
        book = Book()
        picture = Picture()
        moving = False
        selected = -1
        changed = True
        editing = -1
        bucket = False
        layer = "1"

        while running:

            for event in pygame.event.get():
                x, y = pygame.mouse.get_pos()
                l, m, r = pygame.mouse.get_pressed()
                x *= RATIO[0]
                y *= RATIO[1]
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        if editing >= 0:
                            book.blocks[editing].save()
                            book.editing = None
                            editing = -1
                        else:
                            in_menu = not in_menu
                        changed = True
                    if event.key == pygame.K_TAB and not in_menu:
                        in_book = not in_book
                        changed = True
                        book.editing = None
                        editing = -1
                    if (event.key == pygame.K_KP_ENTER or event.key == pygame.K_RETURN) and editing >= 0:
                        book.blocks[editing].save()
                        book.editing = None
                        editing = -1
                        changed = True
                elif editing >= 0:
                    if l == 1 or r == 1:
                        if 560 < x < 1360 and 140 < y < 940:
                            if l == 1 and not bucket:
                                book.blocks[editing].update_wall(x, y, layer)
                                changed = True
                            elif r == 1 and not bucket:
                                book.blocks[editing].update_wall(x, y, "0")
                                changed = True
                            if event.type == pygame.MOUSEBUTTONDOWN:
                                if event.button == 1 and bucket:
                                    book.blocks[editing].bucket(x, y, layer)
                                    changed = True
                                elif event.button == 3 and bucket:
                                    book.blocks[editing].bucket(x, y, "0")
                                    changed = True
                        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                            if 480 < x < 480 + 75 and 140 < y < 140 + 75:
                                book.blocks[editing].fill(layer)
                                changed = True
                            elif 480 < x < 480 + 75 and 220 < y < 220 + 75:
                                book.blocks[editing].fill("0")
                                changed = True
                            elif 480 < x < 480 + 75 and 300 < y < 300 + 75:
                                bucket = False
                            elif 480 < x < 480 + 75 and 380 < y < 380 + 75:
                                bucket = True
                            elif 480 < x < 480 + 75 and 460 < y < 460 + 75:
                                if layer == "1":
                                    layer = "2"
                                    book.set_layer(2)
                                else:
                                    layer = "1"
                                    book.set_layer(1)
                                changed = True
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    if in_menu and event.button == 1:
                        # Cross
                        if 1165 < x < 1165 + 75 and 160 < y < 160 + 75:
                            in_menu = False
                            changed = True
                        # Save
                        elif 789 < x < 789 + 342 and 311 < y < 311 + 135:
                            tk.withdraw()  # we don't want a full GUI, so keep the root window from appearing
                            path = asksaveasfilename(title="Select file", defaultextension=".png", filetypes=(
                            ("PNG (*.png)", "*.png"),
                            ("All Files", "*.*")))  # show an "Save" dialog box and return the path to the selected file
                            if path:
                                in_menu = False
                                changed = True
                                picture.save(path)
                        # Load
                        elif 789 < x < 789 + 342 and 473 < y < 473 + 135:
                            tk.withdraw()  # we don't want a full GUI, so keep the root window from appearing
                            path = askopenfilename(title="Select file", defaultextension=".png", filetypes=(
                            ("PNG (*.png)", "*.png"),
                            ("All Files", "*.*")))  # show an "Open" dialog box and return the path to the selected file
                            if path:
                                in_menu = False
                                changed = True
                                picture.load(path)
                        # Vypnout program
                        elif 789 < x < 789 + 342 and 635 < y < 635 + 135:
                            running = False
                    elif in_book and event.button == 1 and not editing >= 0:
                        # Doleva
                        if 330 < x < 330 + 42 and 890 < y < 890 + 63:
                            if book.page != 0:
                                book.page += -2
                                changed = True
                        # Doprava
                        elif 1530 < x < 1530 + 42 and 890 < y < 890 + 63:
                            if book.page + 2 < (len(book.blocks) + 14) // 15:
                                book.page += 2
                                changed = True
                        # Reload
                        elif 287 < x < 287 + 75 and 38 < y < 38 + 75:
                            book.load()
                            changed = True
                        else:
                            selected = book.select(x, y)
                    elif in_book and event.button == 3 and not editing >= 0:
                        editing = book.select(x, y)
                        changed = True
                    elif not in_menu and not in_book:
                        if event.button == 4:
                            if picture.extend != MAXIMAL_ZOOM:
                                picture.extend += 0.5
                                picture.moveext(x, y, True)
                                changed = True
                        elif event.button == 5:
                            if picture.extend != 1:
                                picture.extend -= 0.5
                                picture.moveext(x, y, False)
                                changed = True
                        elif event.button == 1:
                            if selected >= 0:
                                picture.put(book.blocks[selected], (x * (1 / RATIO[0]), y * (1 / RATIO[1])))
                                changed = True
                        if m == 1:
                            pygame.mouse.get_rel()
                            moving = True
                elif moving and event.type == pygame.MOUSEBUTTONUP:
                    if m == 0:
                        moving = False
                elif moving and not in_menu and not in_book:
                    picture.move(pygame.mouse.get_rel())
                    changed = True
                elif not in_menu and not in_book and pygame.key.get_mods() & pygame.KMOD_CTRL and l == 1:
                    if selected >= 0:
                        picture.put(book.blocks[selected], (x * (1 / RATIO[0]), y * (1 / RATIO[1])))
                        changed = True

            if selected != -1 and not in_book and not in_menu:
                if picture.preview(book.blocks[selected], pygame.mouse.get_pos(), changed):
                    changed = True
            else:
                picture.stop_preview()

            if changed:
                changed = False
                picture.print(self.screen)
                # self.screen.blit(self.image, (0, 0))   #Přidání rámečku
                if in_book:
                    if editing >= 0:
                        book.set_editing(editing)
                    book.draw(self.screen)
                if in_menu:
                    menu.draw(self.screen)

            # Update all game graphics
            pygame.display.update()
            # clock.tick(self.TICK_RATE)


class Picture:

    def __init__(self):
        self.pos = (0, 0)
        self.extend = 1
        self.surf = pygame.Surface((SCREEN_WIDTH, SCREEN_HEIGHT))
        self.pic = pygame.Surface((1920, 1072))
        self.ratio = ((SCREEN_WIDTH / self.pic.get_width()), (SCREEN_HEIGHT / self.pic.get_height()))
        self.pic.fill(WHITE_COLOR)
        self.shown = self.pic.copy()
        self.last_cord = (-1, -1)
        self.walls = [["0"]*1920 for _ in range(1072)]

    def scale(self, pic):
        return pygame.transform.scale(pic, (int(SCREEN_WIDTH * self.extend), int(SCREEN_HEIGHT * self.extend)))

    def load(self, path):
        self.pic = pygame.image.load(path)
        width = self.pic.get_width()
        height = self.pic.get_height()
        self.ratio = ((SCREEN_WIDTH / width), (SCREEN_HEIGHT / height))
        self.shown = self.pic.copy()
        name = path.replace("png", "txt")
        self.walls = []
        if os.path.isfile(name):
            with open(name, 'r', encoding='UTF-8') as f:
                while True:
                    line = f.readline()
                    if line == "":
                        break
                    arr = line.split()
                    self.walls.append(arr)
        else:
            self.walls = [["0"] * width for _ in range(height)]

    def move(self, pos):
        x, y = self.pos
        xm, ym = pos
        xa = 0
        ya = 0
        if x + xm >= 0:
            xa = 0
        elif -SCREEN_WIDTH * (self.extend - 1) >= x + xm:
            xa = -SCREEN_WIDTH * (self.extend - 1)
        else:
            xa = x + xm
        if y + ym >= 0:
            ya = 0
        elif -SCREEN_HEIGHT * (self.extend - 1) >= y + ym:
            ya = -SCREEN_HEIGHT * (self.extend - 1)
        else:
            ya = y + ym
        self.pos = (xa, ya)
        if self.extend == 1:
            self.pos = (0, 0)

    def print(self, screen):
        self.surf.blit(self.shown, self.pos)
        screen.blit(self.surf, (0, 0))

    def moveext(self, x, y, zoom):  # zoom = true -> making bigger; false -> making smaller
        xo, yo = self.pos
        if zoom:
            xm = x * self.extend
            ym = y * self.extend
            self.move((x - xm - xo, y - ym - yo))
        else:
            self.moveext((x - xo) / (self.extend + 0.5), (y - yo) / (self.extend + 0.5), True)

    def save(self, path):
        pygame.image.save(self.pic, path)
        image = pygame.Surface([self.pic.get_width(), self.pic.get_height()], pygame.SRCALPHA, 32)
        image = image.convert_alpha()
        with open(path.replace("png", "txt"), 'w', encoding='UTF-8') as f:
            for line in self.walls:
                text = " ".join(line)
                f.write("%s\n" % text)
        for y in range(len(self.walls)):
            for x in range(len(self.walls[y])):
                if self.walls[y][x] == "2":
                    image.set_at((x, y), self.pic.get_at((x, y)))
        pygame.image.save(image, path.replace(".png", "2.png"))

    def put(self, img, cord):
        x, y = cord
        xp, yp = self.pos
        cord = (
        ((x - xp) // (16 * self.ratio[0] * self.extend)) * 16, ((y - yp) // (16 * self.ratio[1] * self.extend)) * 16)
        img.draw(self.pic, cord, self.walls)
        self.shown = self.scale(self.pic)

    def preview(self, img, cord, changed):
        if self.last_cord != (cord[0] // 16 * self.extend, cord[1] // 16 * self.extend):
            self.last_cord = cord
            x, y = cord
            xp, yp = self.pos
            cord = (((x - xp) // (16 * self.ratio[0] * self.extend)) * 16,
                    ((y - yp) // (16 * self.ratio[1] * self.extend)) * 16)
        if self.last_cord != cord or changed:
            self.shown = self.pic.copy()
            img.draw(self.shown, cord, None)
            self.shown = self.scale(self.shown)
            ret = True
        else:
            ret = False
        return ret

    def stop_preview(self):
        self.shown = self.scale(self.pic)


class Menu:

    def __init__(self):
        self.background = pygame.image.load("images/GUI/board.png")
        self.button1 = pygame.image.load("images/GUI/plank_13.png")
        self.button2 = pygame.image.load("images/GUI/plank_14.png")
        self.button3 = pygame.image.load("images/GUI/plank_15.png")
        self.cross = pygame.image.load("images/GUI/cross.png")
        self.cross = pygame.transform.scale(self.cross, (75, 75))
        self.surf = pygame.Surface((1920, 1080), pygame.SRCALPHA, 32)
        self.surf = self.surf.convert_alpha()

    def draw(self, screen):
        self.surf.blit(self.background, (704, 184))
        self.surf.blit(self.button1, (789, 311))
        self.surf.blit(self.cross, (1165, 160))
        text = font.render("Uložit", True, BLACK_COLOR)
        self.surf.blit(text, (880, 336))
        self.surf.blit(self.button2, (789, 473))
        text = font.render("Načíst", True, BLACK_COLOR)
        self.surf.blit(text, (872, 498))
        self.surf.blit(self.button3, (789, 635))
        text = font.render("Vypnout", True, BLACK_COLOR)
        self.surf.blit(text, (847, 660))
        screen.blit(scale(self.surf), (0, 0))


class Book:

    def __init__(self):
        self.book = pygame.image.load("images/GUI/book.png")
        self.book = pygame.transform.scale(self.book, (1363, 1025))
        self.left = pygame.image.load("images/GUI/left.png")
        self.left = pygame.transform.scale(self.left, (42, 63))
        self.right = pygame.image.load("images/GUI/right.png")
        self.right = pygame.transform.scale(self.right, (42, 63))
        self.reload = pygame.image.load("images/GUI/reload.png")
        self.reload = pygame.transform.scale(self.reload, (75, 75))
        self.fill = pygame.image.load("images/GUI/t_06.png")
        self.fill = pygame.transform.scale(self.fill, (75, 75))
        self.blank = pygame.image.load("images/GUI/t_07.png")
        self.blank = pygame.transform.scale(self.blank, (75, 75))
        self.bucket = pygame.image.load("images/GUI/bucket.png")
        self.bucket = pygame.transform.scale(self.bucket, (75, 75))
        self.feather = pygame.image.load("images/GUI/feather.png")
        self.feather = pygame.transform.scale(self.feather, (75, 75))
        self.layer1 = pygame.image.load("images/GUI/layer1.png")
        self.layer1 = pygame.transform.scale(self.layer1, (75, 75))
        self.layer2 = pygame.image.load("images/GUI/layer2.png")
        self.layer2 = pygame.transform.scale(self.layer2, (75, 75))
        self.layer = self.layer1
        self.editing = None
        self.editing_walls = None
        self.page = 0
        self.blocks = []
        self.load()
        self.surf = pygame.Surface((1920, 1080), pygame.SRCALPHA, 32)
        self.surf = self.surf.convert_alpha()

    def draw(self, screen):
        self.surf.blit(self.book, (278, 27))
        self.draw_icons(self.surf, self.page)
        self.draw_icons(self.surf, self.page + 1)
        self.surf.blit(self.left, (330, 890))
        self.surf.blit(self.right, (1530, 890))
        self.surf.blit(self.reload, (287, 38))
        if self.editing:
            self.surf.blit(self.editing, (560, 140))
            self.surf.blit(self.editing_walls, (560, 140))
            self.surf.blit(self.fill, (480, 140))
            self.surf.blit(self.blank, (480, 220))
            self.surf.blit(self.feather, (480, 300))
            self.surf.blit(self.bucket, (480, 380))
            self.surf.blit(self.layer, (480, 460))
        screen.blit(scale(self.surf), (0, 0))

    def draw_icons(self, screen, page):
        for i in range(5):
            for j in range(3):
                if len(self.blocks) <= 3 * i + j + page * 15:
                    return
                if page % 2 != 0:
                    self.blocks[3 * i + j + page * 15].draw_book(screen, (1050 + j * 175, 125 + i * 175))
                else:
                    self.blocks[3 * i + j + page * 15].draw_book(screen, (400 + j * 175, 125 + i * 175))

    def load(self):
        self.blocks = []
        for name in os.listdir("images/blocks"):
            if name.endswith("png"):
                self.blocks.append(Block(name))

    def select(self, x, y):
        if 400 < x < 500 or 1050 < x < 1150:
            j = 0
        elif 575 < x < 675 or 1225 < x < 1325:
            j = 1
        elif 750 < x < 850 or 1400 < x < 1500:
            j = 2
        else:
            return -1
        if x > 1050:
            j += 15
        if 125 < y < 225:
            i = 0
        elif 300 < y < 400:
            i = 1
        elif 475 < y < 575:
            i = 2
        elif 650 < y < 750:
            i = 3
        elif 825 < y < 925:
            i = 4
        else:
            return -1
        ret = 3 * i + j + self.page * 15
        if ret < len(self.blocks):
            return ret
        else:
            return -1

    def set_editing(self, index):
        self.editing = pygame.transform.scale(self.blocks[index].icon, (800, 800))
        block = self.blocks[index]
        temp = block.icon.copy()
        for y in range(block.height):
            for x in range(block.width):
                if block.walls[y][x] == "1":
                    temp.set_at((x, y), (0, 0, 0, 128))
                elif block.walls[y][x] == "2":
                    temp.set_at((x, y), (255, 0, 0, 128))
        self.editing_walls = pygame.transform.scale(temp, (800, 800))

    def set_layer(self, number):
        if number == 1:
            self.layer = self.layer1
        else:
            self.layer = self.layer2


class Block:

    def __init__(self, name):
        self.icon = pygame.image.load("images/blocks/" + name)
        self.name = name
        self.width, self.height = self.icon.get_size()
        self.walls = []
        self.tested = []
        self.testing = None
        name = name.replace("png", "txt")
        if name in os.listdir("images/blocks"):
            with open("images/blocks/" + name, 'r', encoding='UTF-8') as f:
                while True:
                    line = f.readline()
                    if line == "":
                        break
                    arr = line.split()
                    self.walls.append(arr)
        else:
            self.walls = [["0"] * self.width for _ in range(self.height)]
            with open("images/blocks/" + name, 'w+', encoding='UTF-8') as f:
                for line in self.walls:
                    text = " ".join(line)
                    f.write("%s\n" % text)

    def draw_book(self, screen, cord):
        icon = pygame.transform.scale(self.icon, (100, 100))
        screen.blit(icon, cord)

    def fill(self, to):
        self.walls = [[to] * self.width for _ in range(self.height)]

    def bucket(self, x, y, to):
        x -= 560
        y -= 140
        x = int(x // (800 / self.width))
        y = int(y // (800 / self.height))
        self.testing = self.icon.get_at((x, y))
        self.tested = [[False] * self.width for _ in range(self.height)]
        self.tested[y][x] = True
        self.walls[y][x] = to
        if x - 1 >= 0:
            self.test(x - 1, y, to)
        if y - 1 >= 0:
            self.test(x, y - 1, to)
        if y + 1 < self.height:
            self.test(x, y + 1, to)
        if x + 1 < self.width:
            self.test(x + 1, y, to)

    def test(self, x, y, to):
        self.tested[y][x] = True
        if self.testing == self.icon.get_at((x, y)):
            self.walls[y][x] = to
            if x - 1 >= 0 and not self.tested[y][x-1]:
                self.test(x - 1, y, to)
            if y - 1 >= 0 and not self.tested[y-1][x]:
                self.test(x, y - 1, to)
            if y + 1 < self.height and not self.tested[y+1][x]:
                self.test(x, y + 1, to)
            if x + 1 < self.width and not self.tested[y][x+1]:
                self.test(x + 1, y, to)

    def update_wall(self, x, y, to):
        x -= 560
        y -= 140
        x = int(x//(800/self.width))
        y = int(y//(800/self.height))
        self.walls[y][x] = to

    def draw(self, surf, cord, walls):
        surf.blit(self.icon, cord)
        x, y = cord
        if walls != None and self.walls != [""]:
            for i in range(self.height):
                for j in range(self.width):
                    walls[int(y)+i][int(x)+j] = self.walls[i][j]

    def save(self):
        with open("images/blocks/" + self.name.replace("png", "txt"), 'w+', encoding='UTF-8') as f:
            for line in self.walls:
                text = " ".join(line)
                f.write("%s\n" % text)


pygame.init()

new_game = Game("images/GUI/frame.png", SCREEN_TITLE, SCREEN_WIDTH, SCREEN_HEIGHT)
new_game.run_game_loop()

pygame.quit()
quit()

# pygame.draw.rect(game_screen, BLACK_COLOR, [350, 350, 100, 100])
# pygame.draw.circle(game_screen, BLACK_COLOR, (400, 300), 50)
